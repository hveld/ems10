# © 2018 Hogeschool Rotterdam
# benader cos(x) met de volgende formule cos(x) = 1 - x^2/2! + x^4/4! - x^6/6! + ...
# in deze formule betekent x^4: x tot de macht 4. Dat werkt niet in Python.
# in deze formule betekent 4!: 4 faculteit. Dat werkt niet in Python.
# stap 0; formule herschrijven cos(x) = x^0/0! - x^2/2! + x^4/4! - x^6/6! + ...
# waarbij x de hoek in radialen is
# aantal termen is een variabele die moet worden ingelezen

# stap 1: lees x (floating point getal waarvan de cos bepaald moet worden)
# stap 2: lees aantal termen in (geheel getal > 0)
# stap 3: bepaal nummer voor elke term: 0, 1, 2, 3, ..., aantal_termen-1
# stap 4: bepaal constante voor elke term: 0, 2, 4, 6, ...
# stap 5: bepaal teller van elke term: x^0, x^2, x^4, x^6, ...

x = float(input('Geef de waarde van x: '))
aantal_termen = int(input('Geef het aantal termen: '))
while aantal_termen <= 0:
    aantal_termen = int(input('Geef het aantal termen (een integer ≥ 0): '))

for term_nummer in range(0, aantal_termen):
    term_constante = term_nummer * 2
    term_teller = x ** term_constante
    print('Test: term_teller =', term_teller)

print('cos(' + str(x) + ') benaderd met', aantal_termen, 'termen =', 0.0) # dit is nog niet het goede antwoord!
