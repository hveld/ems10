# © 2018 Hogeschool Rotterdam
# Dit programma leest een geheel getal 0 < n < 10 en drukt
# vervolgens de tafels van 1 tot en met n naast elkaar af

n = int(input('Geef de waarde van n [1..9]: '))
while not 0 < n < 10:
    n = int(input('Geef de waarde van n (minimaal 1 en maximaal 9): '))

print('Test n =', n)
