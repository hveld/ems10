# © 2018 Hogeschool Rotterdam
# Dit programma leest een geheel getal 0 < n < 10 en drukt
# vervolgens de tafels van 1 tot en met n naast elkaar af

n = int(input('Geef de waarde van n [1..9]: '))
while n < 1 or n > 9:
    n = int(input('Geef de waarde van n (minimaal 1 en maximaal 9): '))

for factor in range(1, 11):
    for tafel in range(1, n + 1):
        print(repr(factor).rjust(2) + ' x ' + str(tafel) + ' = ' + repr(factor * tafel).rjust(2), end = ' ');
    print();
