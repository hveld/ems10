def is_digit_valid_in_row(m, d, r):
    if d in m[r]:
        return False
    return True

def print_test_result(test_number, test_result):
    if test_result:
        print('Test', test_number, 'is succesvol uitgevoerd.')
    else:
        print('Test', test_number, 'is Niet succesvol uitgevoerd.')

puzzle = [
    [8, 6, 0, 0, 2, 0, 0, 0, 0],
    [0, 0, 0, 7, 0, 0, 0, 5, 9],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 6, 0, 8, 0, 0],
    [0, 4, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 5, 3, 0, 0, 0, 0, 7],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 6, 0, 0],
    [0, 0, 7, 5, 0, 9, 0, 0, 0]
]

print_test_result(1, not is_digit_valid_in_row(puzzle, 6, 0))
print_test_result(2, is_digit_valid_in_row(puzzle, 6, 1))
print_test_result(3, is_digit_valid_in_row(puzzle, 4, 3))
print_test_result(4, not is_digit_valid_in_row(puzzle, 4, 4))
print_test_result(5, is_digit_valid_in_row(puzzle, 9, 7))
print_test_result(6, not is_digit_valid_in_row(puzzle, 9, 8))
