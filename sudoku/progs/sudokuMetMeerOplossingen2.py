import time

def isDigitValidInRow(m, d, r):
    for c in range(9):
        if m[r][c] == d:
            return False
    return True

def isDigitValidInColumn(m, d, c):
    for r in range(9):
        if m[r][c] == d:
            return False
    return True

def isDigitValidInBlock(m, d, r, c):
    rb = r - r % 3
    cb = c - c % 3
    for r in range(rb, rb + 3):
        for c in range(cb, cb + 3):
            if m[r][c] == d:
                return False
    return True

def isValidMove(m, d, r, c):
    return isDigitValidInRow(m, d, r) and isDigitValidInColumn(m, d, c) and isDigitValidInBlock(m, d, r, c)

def solveHigh(m):
    for r in range(9):
        for c in range(9):
            if m[r][c] == 0:
                for digit in range(9, 0, -1):
                    if isValidMove(m, digit, r, c):
                        m[r][c] = digit
                        if solveHigh(m):
                            return True
                        m[r][c] = 0
                return False
    return True

def printPuzzle(m):
    for r in range(9):
        if r % 3 == 0:
            print(' ------- ------- ------- ')
        for c in range(9):
            if c % 3 == 0:
                print('|', end = ' ')
            digit = m[r][c]
            if digit == 0:
                print(' ', end = ' ')
            else:
                print(m[r][c], end = ' ')
        print('|')
    print(' ------- ------- ------- ')

def printTestResult(testNumber, testResult):
    if testResult:
        print('Test ' + str(testNumber) + ' is succesvol uitgevoerd.')
    else:
        print('Test ' + str(testNumber) + ' is NIET succesvol uitgevoerd.')

puzzle1 = [
    [8, 6, 0, 0, 2, 0, 0, 0, 0],
    [0, 0, 0, 7, 0, 0, 0, 5, 9],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 6, 0, 8, 0, 0],
    [0, 4, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 5, 3, 0, 0, 0, 0, 7],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 6, 0, 0],
    [0, 0, 7, 5, 0, 9, 0, 0, 0]
]
solution1 = [
    [8, 6, 3, 9, 2, 5, 7, 4, 1],
    [4, 1, 2, 7, 8, 6, 3, 5, 9],
    [7, 5, 9, 4, 1, 3, 2, 8, 6],
    [9, 7, 1, 2, 6, 4, 8, 3, 5],
    [3, 4, 6, 8, 5, 7, 9, 1, 2],
    [2, 8, 5, 3, 9, 1, 4, 6, 7],
    [1, 9, 8, 6, 3, 2, 5, 7, 4],
    [5, 2, 4, 1, 7, 8, 6, 9, 3],
    [6, 3, 7, 5, 4, 9, 1, 2, 8]
]
puzzle2 = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0]
]

print('Puzzel 1:')
printPuzzle(puzzle1)
start = time.time()
printTestResult(1, solveHigh(puzzle1) and puzzle1 == solution1)
print('In ' + str(round(time.time() - start, 2)) + ' seconden.')
print('Oplossing puzzel 1:')
printPuzzle(puzzle1)

print('Puzzel 2:')
printPuzzle(puzzle2)
start = time.time()
printTestResult(2, not solveHigh(puzzle2))
print('In ' + str(round(time.time() - start, 2)) + ' seconden.')
print('Oplossing puzzel 2:')
printPuzzle(puzzle2)


