class RowError(Exception):
    pass

class ColumnError(Exception):
    pass

def readPuzzle(filename):
    with open(filename) as textFile:
        puzzle = [[int(d) for d in row.split()] for row in textFile]
    if len(puzzle) != 9:
        raise RowError
    for r in range(9):
        if len(puzzle[r]) != 9:
            raise ColumnError
    return puzzle
   
def testreadPuzzle(filename):
    try:
        puzzle = readPuzzle(filename)
        return puzzle
    except FileNotFoundError:
        print('Error: file niet gevonden.')
    except ValueError:
        print('Error: file bevat niet alleen getallen.')
    except RowError:
        print('Error: file bevat geen 9 rijen.')
    except ColumnError:
        print('Error: file bevat geen 9 kolommen.')

puzzle = testreadPuzzle('sudoku_invalid1.txt')
print(puzzle)
puzzle = testreadPuzzle('sudoku_invalid2.txt')
print(puzzle)
puzzle = testreadPuzzle('sudoku_invalid3.txt')
print(puzzle)
puzzle = testreadPuzzle('sudoku_die_niet_bestaat.txt')
print(puzzle)
puzzle = testreadPuzzle('sudoku_1.txt')
print(puzzle)
