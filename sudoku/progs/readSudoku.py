def printPuzzle(m):
    for r in range(9):
        if r % 3 == 0:
            print(' ------- ------- ------- ')
        for c in range(9):
            if c % 3 == 0:
                print('|', end = ' ')
            digit = m[r][c]
            if digit == 0:
                print(' ', end = ' ')
            else:
                print(m[r][c], end = ' ')
        print('|')
    print(' ------- ------- ------- ')

class RowError(Exception):
    pass

class ColumnError(Exception):
    pass

def readPuzzle(filename):
    with open(filename) as textFile:
        puzzle = [[int(d) for d in row.split()] for row in textFile]
    if len(puzzle) != 9:
        raise RowError
    for r in range(9):
        if len(puzzle[r]) != 9:
            raise ColumnError
    return puzzle

while True:   
    try:
        filenaam = input('Geef filenaam: ')
        puzzle = readPuzzle(filenaam)
        printPuzzle(puzzle)
        break
    except FileNotFoundError:
        print('Error: file niet gevonden.')
    except ValueError:
        print('Error: file bevat niet alleen getallen.')
    except RowError:
        print('Error: file bevat geen 9 rijen.')
    except ColumnError:
        print('Error: file bevat geen 9 kolommen.')
