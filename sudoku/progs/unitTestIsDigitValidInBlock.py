def isDigitValidInBlock(m, d, r, c):
    return True

def printTestResult(testNumber, testResult):
    if testResult:
        print('Test ' + str(testNumber) + ' is succesvol uitgevoerd.')
    else:
        print('Test ' + str(testNumber) + ' is NIET succesvol uitgevoerd.')

puzzle = [
    [8, 6, 0, 0, 2, 0, 0, 0, 0],
    [0, 0, 0, 7, 0, 0, 0, 5, 9],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 6, 0, 8, 0, 0],
    [0, 4, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 5, 3, 0, 0, 0, 0, 7],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 6, 0, 0],
    [0, 0, 7, 5, 0, 9, 0, 0, 0]
]

printTestResult(1, not isDigitValidInBlock(puzzle, 6, 6, 6))
printTestResult(2, not isDigitValidInBlock(puzzle, 6, 7, 7))
printTestResult(3, not isDigitValidInBlock(puzzle, 6, 6, 8))
printTestResult(4, isDigitValidInBlock(puzzle, 3, 6, 3))
printTestResult(5, isDigitValidInBlock(puzzle, 3, 7, 4))
printTestResult(6, isDigitValidInBlock(puzzle, 3, 6, 5))
