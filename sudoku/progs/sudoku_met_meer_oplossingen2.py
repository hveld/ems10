import time

def is_digit_valid_in_row(m, d, r):
    for c in range(9):
        if m[r][c] == d:
            return False
    return True

def is_digit_valid_in_column(m, d, c):
    for r in range(9):
        if m[r][c] == d:
            return False
    return True

def is_digit_valid_in_block(m, d, r, c):
    rb = r - r % 3
    cb = c - c % 3
    for r in range(rb, rb + 3):
        for c in range(cb, cb + 3):
            if m[r][c] == d:
                return False
    return True

def is_valid_move(m, d, r, c):
    return is_digit_valid_in_row(m, d, r) and is_digit_valid_in_column(m, d, c) and is_digit_valid_in_block(m, d, r, c)

def solve_high(m):
    for r in range(9):
        for c in range(9):
            if m[r][c] == 0:
                for digit in range(9, 0, -1):
                    if is_valid_move(m, digit, r, c):
                        m[r][c] = digit
                        if solve_high(m):
                            return True
                m[r][c] = 0
                return False
    return True

def print_puzzle(m):
    for r in range(9):
        if r % 3 == 0:
            print(' ------- ------- ------- ')
        for c in range(9):
            if c % 3 == 0:
                print('|', end=' ')
            digit = m[r][c]
            if digit == 0:
                print(' ', end=' ')
            else:
                print(m[r][c], end=' ')
        print('|')
    print(' ------- ------- ------- ')

def print_test_result(test_number, test_result):
    if test_result:
        print('Test', test_number, 'is succesvol uitgevoerd.')
    else:
        print('Test', test_number, 'is Niet succesvol uitgevoerd.')

puzzle1 = [
    [8, 6, 0, 0, 2, 0, 0, 0, 0],
    [0, 0, 0, 7, 0, 0, 0, 5, 9],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 6, 0, 8, 0, 0],
    [0, 4, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 5, 3, 0, 0, 0, 0, 7],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 6, 0, 0],
    [0, 0, 7, 5, 0, 9, 0, 0, 0]
]
solution1 = [
    [8, 6, 3, 9, 2, 5, 7, 4, 1],
    [4, 1, 2, 7, 8, 6, 3, 5, 9],
    [7, 5, 9, 4, 1, 3, 2, 8, 6],
    [9, 7, 1, 2, 6, 4, 8, 3, 5],
    [3, 4, 6, 8, 5, 7, 9, 1, 2],
    [2, 8, 5, 3, 9, 1, 4, 6, 7],
    [1, 9, 8, 6, 3, 2, 5, 7, 4],
    [5, 2, 4, 1, 7, 8, 6, 9, 3],
    [6, 3, 7, 5, 4, 9, 1, 2, 8]
]
puzzle2 = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0]
]

print('Puzzel 1:')
print_puzzle(puzzle1)
start = time.time()
print_test_result(1, solve_high(puzzle1) and puzzle1 == solution1)
print('In', round(time.time() - start, 2), 'seconden.')
print('Oplossing puzzel 1:')
print_puzzle(puzzle1)

print('Puzzel 2:')
print_puzzle(puzzle2)
start = time.time()
print_test_result(2, not solve_high(puzzle2))
print('In', round(time.time() - start, 2), 'seconden.')
print('Oplossing puzzel 2:')
print_puzzle(puzzle2)
