def isDigitValidInDiagonals(m, d, r, c):
    if r == c:
        for i in range(9):
            if m[i][i] == d:
                return False
    elif r == (8 - c):
        for i in range(9):
            if m[i][8 - i] == d:
                return False
    return True

def printTestResult(testNumber, testResult):
    if testResult:
        print('Test ' + str(testNumber) + ' is succesvol uitgevoerd.')
    else:
        print('Test ' + str(testNumber) + ' is NIET succesvol uitgevoerd.')

puzzle = [
    [8, 6, 0, 0, 2, 0, 0, 0, 0],
    [0, 0, 0, 7, 0, 0, 0, 5, 9],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 6, 0, 8, 0, 0],
    [0, 4, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 5, 3, 0, 0, 0, 0, 7],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 6, 0, 0],
    [0, 0, 7, 5, 0, 9, 0, 0, 0]
]

printTestResult(1, not isDigitValidInDiagonals(puzzle, 8, 4, 4))
printTestResult(2, not isDigitValidInDiagonals(puzzle, 8, 5, 5))
printTestResult(3, not isDigitValidInDiagonals(puzzle, 2, 0, 8))
printTestResult(4, isDigitValidInDiagonals(puzzle, 1, 0, 8))
printTestResult(5, isDigitValidInDiagonals(puzzle, 4, 4, 4))
printTestResult(6, isDigitValidInDiagonals(puzzle, 4, 8, 8))
