import time
import copy

def is_digit_valid_in_row(m, d, r):
    for c in range(9):
        if m[r][c] == d:
            return False
    return True

def is_digit_valid_in_column(m, d, c):
    for r in range(9):
        if m[r][c] == d:
            return False
    return True

def is_digit_valid_in_block(m, d, r, c):
    rb = r - r % 3
    cb = c - c % 3
    for r in range(rb, rb + 3):
        for c in range(cb, cb + 3):
            if m[r][c] == d:
                return False
    return True

def is_digit_valid_in_diagonals(m, d, r, c):
    if r == c:
        for i in range(9):
            if m[i][i] == d:
                return False
    elif r == (8 - c):
        for i in range(9):
            if m[i][8 - i] == d:
                return False
    return True

def is_valid_move(m, d, r, c):
    return is_digit_valid_in_row(m, d, r) and is_digit_valid_in_column(m, d, c) and is_digit_valid_in_block(m, d, r, c) and is_digit_valid_in_diagonals(m, d, r, c)

def solve_low(m):
    for r in range(9):
        for c in range(9):
            if m[r][c] == 0:
                for digit in range(1, 10):
                    if is_valid_move(m, digit, r, c):
                        m[r][c] = digit
                        if solve_low(m):
                            return True
                m[r][c] = 0
                return False
    return True

def solve_high(m):
    for r in range(9):
        for c in range(9):
            if m[r][c] == 0:
                for digit in range(9, 0, -1):
                    if is_valid_move(m, digit, r, c):
                        m[r][c] = digit
                        if solve_high(m):
                            return True
                m[r][c] = 0
                return False
    return True

def solve(puzzle):
    result1 = copy.deepcopy(puzzle)
    if solve_low(result1):
        result2 = copy.deepcopy(puzzle)
        if solve_high(result2) and result1 == result2:
            for r in range(9):
                for c in range(9):
                    puzzle[r][c] = result1[r][c]
            return True
    return False

def print_puzzle(m):
    for r in range(9):
        if r % 3 == 0:
            print(' ------- ------- ------- ')
        for c in range(9):
            if c % 3 == 0:
                print('|', end=' ')
            digit = m[r][c]
            if digit == 0:
                print(' ', end=' ')
            else:
                print(m[r][c], end=' ')
        print('|')
    print(' ------- ------- ------- ')

class RowError(Exception):
    pass

class ColumnError(Exception):
    pass

def read_puzzle(filename):
    with open(filename) as text_file:
        puzzle = [[int(d) for d in row.split()] for row in text_file]
    if len(puzzle) != 9:
        raise RowError
    for r in range(9):
        if len(puzzle[r]) != 9:
            raise ColumnError
    return puzzle

while True:   
    try:
        filenaam = input('Geef filenaam: ')
        puzzle = read_puzzle(filenaam)
        print_puzzle(puzzle)
        start = time.time()
        if solve(puzzle):
            print('Oplossing:')
            print_puzzle(puzzle)
            print('Gevonden in', round(time.time() - start, 2), 'seconden.')
        else:
            print('Geen unieke oplossing gevonden in', round(time.time() - start, 2), 'seconden.')
        break
    except FileNotFoundError:
        print('Error: file niet gevonden.')
    except ValueError:
        print('Error: file bevat niet alleen getallen.')
    except RowError:
        print('Error: file bevat geen 9 rijen.')
    except ColumnError:
        print('Error: file bevat geen 9 kolommen.')

