def isDigitValidInRow(m, d, r):
    for c in range(9):
        if m[r][c] == d:
            return False
    return True

def isDigitValidInColumn(m, d, c):
    for r in range(9):
        if m[r][c] == d:
            return False
    return True

def isDigitValidInBlock(m, d, r, c):
    rb = r - r % 3
    cb = c - c % 3
    for r in range(rb, rb + 3):
        for c in range(cb, cb + 3):
            if m[r][c] == d:
                return False
    return True

def isValidMove(m, d, r, c):
    return isDigitValidInRow(m, d, r) and isDigitValidInColumn(m, d, c) and isDigitValidInBlock(m, d, r, c)

def printTestResult(testNumber, testResult):
    if testResult:
        print('Test ' + str(testNumber) + ' is succesvol uitgevoerd.')
    else:
        print('Test ' + str(testNumber) + ' is NIET succesvol uitgevoerd.')

puzzle = [
    [8, 6, 0, 0, 2, 0, 0, 0, 0],
    [0, 0, 0, 7, 0, 0, 0, 5, 9],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 6, 0, 8, 0, 0],
    [0, 4, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 5, 3, 0, 0, 0, 0, 7],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 0, 0, 0, 6, 0, 0],
    [0, 0, 7, 5, 0, 9, 0, 0, 0]
]

printTestResult(1, not isValidMove(puzzle, 6, 0, 2))
printTestResult(2, isValidMove(puzzle, 6, 2, 5))
printTestResult(3, not isValidMove(puzzle, 4, 3, 0))
printTestResult(4, not isValidMove(puzzle, 4, 4, 0))
printTestResult(5, not isValidMove(puzzle, 4, 5, 0))
printTestResult(6, isValidMove(puzzle, 9, 7, 7))

