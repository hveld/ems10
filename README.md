# EMS10 - Embedded Systems 1 #

Dit repository is bedoeld voor studenten en docenten van de opleiding Elektrotechniek van de Hogeschool Rotterdam en wordt gebruikt om aanvullend studiemateriaal voor de module "EMS10 - Embedded Systems 1" te ontwikkelen en te verspreiden. 

Alle informatie is te vinden op de [Wiki](https://bitbucket.org/HR_ELEKTRO/ems10/wiki/).