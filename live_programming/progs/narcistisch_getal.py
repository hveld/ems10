def aantal_cijfers(getal):
    cijfers = 1
    getal = getal // 10
    while getal > 0:
        cijfers = cijfers + 1
        getal = getal // 10
    return cijfers

def is_narcistisch(getal):
    begin_getal = getal
    n = aantal_cijfers(getal)
    som = 0
    for i in range(n):
        cijfer = getal % 10
        som = som + cijfer**n 
        getal = getal // 10
    return som == begin_getal

def kleinste_narcistische_getal_met_n_cijfers(n):
    if n == 1:
        return 0
    for kandidaat in range(10 ** (n - 1), 10 ** n):
        if is_narcistisch(kandidaat):
            return kandidaat
    return 'Bestaat niet'

if aantal_cijfers(135) != 3:
    print('Error: aantal_cijfers(135) != 3')
if aantal_cijfers(153) != 3:
    print('Error: aantal_cijfers(153) != 3')
if aantal_cijfers(4679307774) != 10:
    print('Error: aantal_cijfers(4679307774) != 10')
if aantal_cijfers(115132219018763992565095597973971522400) != 39:
    print('Error: aantal_cijfers(115132219018763992565095597973971522400) != 39')

if is_narcistisch(135) != False:
    print('Error: is_narcistisch(135) != False')
if is_narcistisch(153) != True:
    print('Error: is_narcistisch(153) != True')
if is_narcistisch(4679307774) != True:
    print('Error: is_narcistisch(4679307774) != True')
if is_narcistisch(115132219018763992565095597973971522400) != True:
    print('Error: is_narcistisch(115132219018763992565095597973971522400) != True')
if is_narcistisch(1151322190187639925650955979739715224002) != False:
    print('Error: is_narcistisch(1151322190187639925650955979739715224002) != False')

if kleinste_narcistische_getal_met_n_cijfers(3) != 153:
    print('Error: kleinste_narcistische_getal_met_n_cijfers(3) != 153')
if kleinste_narcistische_getal_met_n_cijfers(5) != 54748:
    print('Error: kleinste_narcistische_getal_met_n_cijfers(3) != 54748')

for aantalcijfers in range(1, 8):
    print('Het kleinste narcistische getal met', aantalcijfers, 'cijfers:', kleinste_narcistische_getal_met_n_cijfers(aantalcijfers))