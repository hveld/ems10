import math

def berekenWortels(a, b, c):
    # Vul hier je code in
    return None

wortels = berekenWortels(int(input('Geef a: ')), int(input('Geef b: ')), int(input('Geef c: ')))
aantalWortels = len(wortels)
if aantalWortels == 0:
    print('Deze vierkantsvergelijking heeft geen reële wortels.')
elif aantalWortels == 1:
    print('Deze vierkantsvergelijking heeft één reële wortel:')
    print(wortels[0])
else:
    print('Deze vierkantsvergelijking heeft twee reële wortels:')
    print(wortels[0], wortels[1])
