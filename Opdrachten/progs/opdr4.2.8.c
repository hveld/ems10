#include <msp430.h> 


/**
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD; // Stop de watchdog timer

	P1OUT = 0x00; // Register resetten
	P1DIR = 0x03; // P1.0 en P1.1 als output instellen
	P1OUT = 0x01; // Led 1 aan
	P1OUT = 0x02; // Led 2 aan

	return 0;
}
