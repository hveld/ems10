#include <msp430.h> 

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	
	P1DIR = 0b00000011;
	P1OUT = 0b00000011;
	
	return 0;
}
