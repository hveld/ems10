/*  aantal hulp functies voor getallen en tekst*/
/* draai array om */
inline static void reverse( char s[] )
{
    unsigned int i, j ;
    char c ;

    //tegelijk optellen en aftellen
    for (i = 0, j = strlen(s)-1; i < j ; i++, j-- )
    {
        c = s[i] ;
        s[i] = s[j] ;
        s[j] = c ;
    }
}

/* itoa:  integer naar ascii omzetting */
inline void itoa( int n, char s[] )
{
    int i, sign ;

    //als negatief
    if( (sign = n) < 0 )
    {
      n = -n; //maak positief
    }

    i = 0;

    do
    {
        s[i++] = n % 10 + '0';//offset vanaf '0' character
    }while( (n /= 10)>0 );   //delen door 10 en opnieuw

    //als negatief
    if(sign < 0)
    {
        s[i++] = '-'; //voeg - toe
    }

    s[i] = '\0'; //eindigen string

    reverse(s); //draai de boel om
}

#pragma vector = ADC10_VECTOR
__interrupt void adc10isr(void)
{
    volatile char tekst[5];
    itoa(ADC10MEM, tekst);
}