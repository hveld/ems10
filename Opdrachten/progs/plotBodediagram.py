import numpy as np
import matplotlib.pyplot as plt

def H1(w):
    return 1j * w * 22e-3 / (1e2 + 1j * w * 22e-3)

def H2(w):
    return (1/(1j * w * 1e-6) + 1j * w * 22e-3) / (100 + 1/(1j * w * 1e-6) + 1j * w * 22e-3)

def plotBodediagram(H):
    # vul hier je code in
    pass
    
plt.figure(1)
plotBodediagram(H1)
plt.figure(2)
plotBodediagram(H2)
plt.show()

