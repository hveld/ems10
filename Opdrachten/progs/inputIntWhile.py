def canConvertStrToType(str, type):
    try:
        type(str)
    except:
        return False
    return True

def inputInt(prompt, min, max):
    i = input(prompt)
    while not canConvertStrToType(i, int) or int(i) < min or int(i) > max:
        i = input(prompt)
    return int(i)
        
r1 = inputInt('Geef het eerste toetsresultaat: ', 1, 10)
r2 = inputInt('Geef het tweede toetsresultaat: ', 1, 10)
print('Het gemiddelde is: ' + str((r1 + r2) / 2))

