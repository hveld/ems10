import numpy as np
import matplotlib.pyplot as plt

def H1(w):
    return 1j*w*22e-3 / (1e2 + 1j*w*22e-3)

def H2(w):
    return (1/(1j*w*1e-6) + 1j*w*22e-3) / (100 + 1/(1j*w*1e-6) + 1j*w*22e-3)

def plot_bodediagram(h):
    # vul hier je code in
    pass
    
plt.figure(1)
plot_bodediagram(H1)
plt.figure(2)
plot_bodediagram(H2)
plt.show()
