#include <msp430.h>

void maak_array4_nul(int a[])
{
    int i;
    for (i = 0; i < 4; i++)
    {
        a[i] = 0;
    }
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer
    int rij1[4] = {1, 2, 3, 4};
    maak_array4_nul(rij1);
    return 0;
}
