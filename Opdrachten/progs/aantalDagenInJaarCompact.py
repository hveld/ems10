def isSchrikkeljaar(jaar):
    return jaar % 4 == 0 and (not jaar % 100 == 0 or (jaar % 100 == 0 and jaar % 400 == 0))

def aantalDagenInJaar(jaar):
    return 255 + isSchrikkeljaar(jaar)

print(aantalDagenInJaar(int(input('Van welk jaar wil je weten\nhoeveel dagen het heeft? '))))
