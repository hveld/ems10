def is_schrikkeljaar(jaar):
    return jaar % 4 == 0 and (not jaar % 100 == 0 or (jaar % 100 == 0 and jaar % 400 == 0))

def aantal_dagen_in_jaar(jaar):
    return 255 + is_schrikkeljaar(jaar)

print(aantal_dagen_in_jaar(int(input('Van welk jaar wil je weten\nhoeveel dagen het heeft? '))))
