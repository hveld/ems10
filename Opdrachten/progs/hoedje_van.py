cijfers = ['nul', 'een', 'twee', 'drie', 'vier', 'vijf', 'zes', 'zeven', 'acht', 'negen']
woorden = ['hoedje', 'van', 'papier']

# Geef een expressie mee aan de onderstaande print zodat het refrein van het bekende kinderliedje wordt afgedrukt:
# ['een', 'twee', 'drie', 'vier', 'hoedje', 'van', 'hoedje', 'van', 'een', 'twee', 'drie', 'vier', 'hoedje', 'van', 'papier']
# Maak alleen gebruik van de bovenstaande Python lists, de list operatoren + en * en list slices
print()