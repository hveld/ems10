def printIets(n):
    lsb = n % 2
    if n > 0:
        printIets(n // 2)
        print(lsb, end = '')
    else:
        print()
     
printIets(10)

printIets(2)
printIets(3)
printIets(9)
printIets(15)
