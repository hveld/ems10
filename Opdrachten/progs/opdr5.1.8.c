#include <msp430.h> 

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer
    P2DIR |= 1<<2 | 1<<1 | 1<<0; // zet pins P2.2, P2.1 en P2.0 op output
    P2OUT &= ~(1<<2 | 1<<1 | 1<<0); // maak pins P2.2, P2.1 en P2.0 laag
    P1DIR &= ~(1<<0); // zet pin P1.0 op intput
    P1REN |= 1<<0; // zet interne weerstand aan bij pin P1.0
    P1OUT &= ~(1<<0); // selecteer pull down weerstand op pin P1.0.
	
    while (1)
    {
        while ((P1IN & 1<<0) == 0) /* leeg */; // wacht tot P1.0 wordt ingedrukt
        while ((P1IN & 1<<0) != 0) /* leeg */; // wacht tot P1.0 wordt losgelaten
        while ((P1IN & 1<<0) == 0) /* leeg */; // wacht tot P1.0 wordt ingedrukt
        while ((P1IN & 1<<0) != 0) /* leeg */; // wacht tot P1.0 wordt losgelaten
        while ((P1IN & 1<<0) == 0) /* leeg */; // wacht tot P1.0 wordt ingedrukt
        P2OUT ^= 1<<0; // inverteer de led
        while ((P1IN & 1<<0) != 0) /* leeg */; // wacht tot P1.0 wordt losgelaten
    }

	return 0;
}